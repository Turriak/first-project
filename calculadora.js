function Sumar() {
    const num1 = document.querySelector('.numero1').value;
    const num2 = document.querySelector('.numero2').value;
    const result = parseInt(num1) + parseInt(num2);
    alert(result);
    console.log(result);
}

function Restar() {
    const num1 = document.querySelector('.numero1').value;
    const num2 = document.querySelector('.numero2').value;
    const result = parseInt(num1) - parseInt(num2);
    alert(result);
    console.log(result);
}

function Multiplicar() {
    const num1 = document.querySelector('.numero1').value;
    const num2 = document.querySelector('.numero2').value;
    const result = parseInt(num1) * parseInt(num2);
    alert(result);
    console.log(result);;
}

function Dividir() {
    const num1 = document.querySelector('.numero1').value;
    const num2 = document.querySelector('.numero2').value;
    const result = parseInt(num1) / parseInt(num2);
    alert(result);
    console.log(result);
}

document.querySelector('.sumar').addEventListener('click', Sumar);
document.querySelector('.restar').addEventListener('click', Restar);
document.querySelector('.multiplicar').addEventListener('click', Multiplicar);
document.querySelector('.dividir').addEventListener('click', Dividir);